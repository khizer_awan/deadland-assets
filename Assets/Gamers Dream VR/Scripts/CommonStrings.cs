﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CommonStrings {




    /// <summary>
    /// ////////////////////////////////////////TAGS ////////////////////////////////////////////////////////////
    /// </summary>
	public static String PLAY_AREA_TAG = "PlayArea";
	public static String SHOOTER_AREA_TAG = "ShooterArea";
	public static String ZOMBIE_TAG="zombie";
	public static String ZOMBIE_HAND_TAG="zombie_hand";
	public static String SPAWN_POINT_TAG="SpawnPoint";
	public static String PLAYER_TAG="MainCamera";


    /// <summary>
    /// //////////////////////////////////////// ANIMATION KEYS ////////////////////////////////////////////////////////////
    /// </summary>
    public static String KEY_SLIDE_IN = "SlideIn";
    public static String KEY_SLIDE_OUT = "SlideOut";

}
