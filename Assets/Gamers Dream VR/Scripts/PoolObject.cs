﻿using UnityEngine;
using System.Collections;

public class PoolObject : MonoBehaviour {

	public float destroyObjectAfter=5f;


	void  Start(){

		StartCoroutine (destroyObject(destroyObjectAfter));

	}


	public virtual void OnObjectReuse() {

		if (this.gameObject.activeSelf) {
			gameObject.SetActive (false);
		}
		gameObject.SetActive (true);


	}

	protected void Destroy() {
		gameObject.SetActive (false);
	}



	IEnumerator destroyObject(float timeout){

		yield return new WaitForSeconds (timeout);

		gameObject.SetActive (false);

	}

}
