﻿
namespace VRTK.Examples
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class PistolController : VRTK_InteractableObject
    {

        [Header("Components")]
        public Animation anim;
        public Transform aim;

        [Header("Gun's Variables")]
        public int range;
        public int damage;
        public float hitEffectDistrcutionTime;

        [Range(0, 100)]
        public float triggerForce;

        [Header("Effects")]
        [Tooltip("Bullet Fire effect")]
        public ParticleSystem fireMuzzle;
        [Tooltip("Effect initialized at point where bullet hits")]
        public GameObject bulletHitStatic, bulletHitBody;

        [Header("Audio clips")]
        public AudioClip gunShotClip;

        [Header("Pointer Canvas")]
        public GameObject pointerCanvas; // an image pointing towards this object when this game is somwhere on ground


        public bool isTesting = false;


        private AudioSource speaker;
        private Animator animator;




        private VRTK_ControllerReference controllerReference;


        protected override void Awake()
        {
            base.Awake();
            speaker = GetComponent<AudioSource>();
            animator = GetComponent<Animator>();
            //		lineRenderer = GetComponent<LineRenderer> ();
        }


        // Use this for initialization
        void Start()
        {
          


        }


        public override void Grabbed(VRTK_InteractGrab currentGrabbingObject)
        {
            base.Grabbed(currentGrabbingObject);
            controllerReference = VRTK_ControllerReference.GetControllerReference(currentGrabbingObject.controllerEvents.gameObject);
            pointerCanvas.SetActive(false);

        }

        public override void Ungrabbed(VRTK_InteractGrab previousGrabbingObject)
        {
            base.Ungrabbed(previousGrabbingObject);
            controllerReference = null;
            pointerCanvas.SetActive(true);
        }



        public override void StartUsing(VRTK_InteractUse currentUsingObject)
        {
            base.StartUsing(currentUsingObject);

            Shoot();


        }

        public override void StopUsing(VRTK_InteractUse previousUsingObject)
        {
            base.StopUsing(previousUsingObject);
           // Shoot();

        }


        // Update is called once per frame
        protected override void FixedUpdate()
        {
            base.FixedUpdate();

            anim["guntrigger"].speed = 0;
            anim["guntrigger"].time = triggerForce / 100;
            anim.Play("guntrigger");

            if (triggerForce > 99)
            {
                Shoot();
            }

 
            if (Input.GetKeyDown(KeyCode.Space))
            {
                print("shoot");
                Shoot();

            }

           
        }
        protected override void Update()
        {
            base.Update();
 
            if (Input.GetKeyDown(KeyCode.Space))
            { 
                Shoot();

            }


        }


 

        private void Shoot()
        {

            animator.SetTrigger("shoot");
            triggerForce = 0;

            speaker.PlayOneShot(gunShotClip);

            fireMuzzle.Stop();
            fireMuzzle.Play();


            if (aim)
            {

                RaycastHit hit;

                //get range from the ShootingSystem script
                if (Physics.Raycast(aim.position, aim.forward, out hit, range))
                {

                    //				lineRenderer.SetPosition (1, new Vector3 (0, 0, hit.distance));//if raycast hit somewhere then stop laser effect at that point
                    Fire(hit, hit.collider.gameObject);//if hit some point then shoot through shootingSystem Fire Function

                }
            }
        }

        public void Fire(RaycastHit hitPoint, GameObject hitObject)
        { //with hit effect

            //hitObject.SendMessage("ApplyDamage", damage, SendMessageOptions.DontRequireReceiver);
           // GameObject clone;
           // Quaternion rotation = Quaternion.FromToRotation(Vector3.up, hitPoint.normal);
            if (hitObject.tag.Equals(CommonStrings.ZOMBIE_TAG))
            {
               // GameObject hitObjectParent = hitObject.transform.root.gameObject;
               // hitObjectParent.GetComponent<DamageManager>().ApplyDamage(damage,transform);
               // hitObjectParent.SendMessage("ApplyDamage", damage, SendMessageOptions.DontRequireReceiver);
                Hit_Body bodyhit = hitObject.GetComponent<Hit_Body>();
                //						print ("hit object name "+hitObject);
                if (bodyhit != null)
                {
                    bodyhit.OnHit(hitPoint, transform, damage);
                }
                // clone = Instantiate(bulletHitBody, hitPoint.point, rotation, hitObject.transform) as GameObject;
            }
            else
            {
              //  clone = Instantiate(bulletHitStatic, hitPoint.point, rotation, hitObject.transform) as GameObject;
            }

        //    Destroy(clone, hitEffectDistrcutionTime);



        }



    }
}