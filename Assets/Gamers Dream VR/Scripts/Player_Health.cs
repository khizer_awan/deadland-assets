﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Health : MonoBehaviour {


	public int health = 100; 
	public GameObject damageEffect;
    private GameObject effect;
	private ParticleSystem blood;


	public static Player_Health intance;


	public void Awake(){
		intance = this;
       // effect = Instantiate(damageEffect,Vector3.zero,Quaternion.identity);
	//	blood = effect.GetComponent<ParticleSystem>();

	}


	public void ApplyDamage(int damage){

//		print ("health "+health);

		health -= damage;

		if (health <= 0) {
		
			PlayerDead ();
		
		}

		DamageEffects ();


	}


	private void DamageEffects(){

		print("player getting hit");
        if (blood == null) {
            effect = Instantiate(damageEffect, Vector3.zero, Quaternion.identity);
            blood = effect.GetComponent<ParticleSystem>();
        }

		blood.transform.position = new Vector3(transform.position.x,transform.position.y,transform.position.z+0.15f);
		//blood.transform.position.z += 1f;
		blood.Play();
		//effects when player gets damage;
	}

	private void PlayerDead(){
		//effects when player Dies
	}





}
