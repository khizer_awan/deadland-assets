﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this script will put all the necesory components to the main camera( which basicaly is player).. and will manage all the stuff.
public class PlayerManager : MonoBehaviour {

    [Header("Player Properties")]
    public float mass = 0.01f;
    public bool isKinematic = true;
    public bool useGravity = false;

    public Vector3 colliderCenter;
    public Vector3 colliderSize;

    public GameObject bloodSplashPlayer;




    private GameObject player;
    private bool haveFoundPlayer;

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {

        if (!haveFoundPlayer)
        {
            Camera m_camera = Camera.main;
            if (m_camera != null)
            {
                player = m_camera.gameObject;
                haveFoundPlayer = true;
                SetUpPlayer();
            }
        }
        else {
            
        }
    }


    void SetUpPlayer() {
        //RIGIDBODY
        Rigidbody body =  player.AddComponent<Rigidbody>();
        body.mass = mass;
        body.isKinematic = isKinematic;
        body.useGravity = useGravity;

        //ADD HEALTH SCRPT
        Player_Health health = player.AddComponent<Player_Health>();
        health.damageEffect = bloodSplashPlayer;


        //Add collider
        BoxCollider mcollider =  player.AddComponent<BoxCollider>();
        mcollider.size = colliderSize;
        mcollider.center = colliderCenter;

    }


    public GameObject GetPlayer() {

        return player;
    }



    //sb se pehle backup krlo aur phr ruins wale k materials dobara import kro.... 
    //just to check if those materials are more better i quality..
    //keep them high quality unless you get a performance issue

    
}
