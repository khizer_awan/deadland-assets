﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))] 
[RequireComponent(typeof(Animator))]
public class Enemy_Behavior : MonoBehaviour {



	private float stoppingDistance=3f;


	private NavMeshAgent agent;
	private GameManager manager;
	private Animator anim;
	private Transform currentDestination;
	protected DamageManager damageManager;

	protected enum EnemyMode{
		Normal,
		GettingHit,
		Aggressive,
		Dead

	}
	protected EnemyMode enemyMode = EnemyMode.Normal;


	void Awake(){
		agent = GetComponent<NavMeshAgent> ();
		manager = FindObjectOfType<GameManager> ();
		anim = GetComponent<Animator> ();
		currentDestination = GameObject.FindGameObjectWithTag (CommonStrings.PLAY_AREA_TAG).transform;
		damageManager = GetComponent<DamageManager> ();
	}


	// Use this for initialization
	public virtual void Start () { 
		agent.SetDestination (currentDestination.position);


	}
	
	// Update is called once per frame
	public virtual void Update () {
		
		switch(enemyMode){

		case EnemyMode.Normal:
			{
				NormalMovement ();
				break;}
		case EnemyMode.Aggressive:
			{
				
				break;}
		case EnemyMode.GettingHit:
			{

				break;}
		case EnemyMode.Dead:
			{

				break;}
		default:{
				
				break;}

		}
	}
		
	public virtual void OnTriggerEnter(Collider otherCol){ 
  
		if (otherCol.tag.Equals (CommonStrings.PLAY_AREA_TAG)) {
			currentDestination = GameObject.FindGameObjectWithTag (CommonStrings.PLAYER_TAG).transform;
			if (!currentDestination) {
				currentDestination = FindObjectOfType<Player_Health> ().gameObject.transform;
			}
		}
			
	}


	private void NormalMovement(){
		if (Vector3.Distance (transform.position,currentDestination.position ) > agent.stoppingDistance+0.8) {
			agent.SetDestination (currentDestination.position);
			anim.SetBool ("Walk",true);

		} else {

		var lookPos = currentDestination.position - transform.position;
		lookPos.y = 0;
		var rotation = Quaternion.LookRotation(lookPos);
		transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 5f);

			anim.SetBool ("Walk",false);
			anim.SetTrigger ("Attack");
		}
	}

//	zombie k sth se culti maar lo to wo murhta nai ha.. you can sort it out.

	public virtual void Aggressive(){
		
	}




}
