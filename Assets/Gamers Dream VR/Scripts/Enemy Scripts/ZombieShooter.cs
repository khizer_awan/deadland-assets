﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Security.AccessControl;


[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Animator))]
public class ZombieShooter : MonoBehaviour
{


    public Transform shooterGun;
    public GameObject EnemyGolla;

    private float stoppingDistance = 3f;


    private NavMeshAgent agent;
    private GameManager manager;
    private Animator anim;
    private Transform currentDestination;
    protected DamageManager damageManager;

    private GameObject[] spawners;


    public GameObject player;


    protected enum EnemyMode
    {
        Normal,
        GettingHit,
        Aggressive,
        Dead

    }
    protected EnemyMode enemyMode = EnemyMode.Normal;


    void Awake()
    {

        spawners = GameObject.FindGameObjectsWithTag(CommonStrings.SHOOTER_AREA_TAG);
        agent = GetComponent<NavMeshAgent>();

        //player = GameObject.FindGameObjectWithTag (CommonStrings.PLAYER_TAG);
        //if (!player)
        //player = Player_Health.intance;
        //	player = GameObject.FindObjectOfType<Player_Health> ().gameObject;




        manager = FindObjectOfType<GameManager>();
        anim = GetComponent<Animator>();
        damageManager = GetComponent<DamageManager>();
    }



    // Use this for initialization
    void Start()
    {


        FindNearestShootingPos();


        agent.SetDestination(currentDestination.position);

        Invoke("InititializePlayer", 3f);


        //		print ("destination "+agent.destination);
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        Movement();

    }

    private void Movement()
    {

        if (Vector3.Distance(transform.position, currentDestination.position) > agent.stoppingDistance)
        {
            //print ("check ");
            agent.SetDestination(currentDestination.position);
            anim.SetTrigger("Walk");

        }
        else
        {

            var lookPos = player.transform.position - transform.position;
            lookPos.y = 0;
            var rotation = Quaternion.LookRotation(lookPos);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 5f);
            anim.SetTrigger("Attack");
        }
    }

    //	make hitting colliders and dmage scripts for shooter demon... make the animation speed and etc little more accurate and there you go.

    void FindNearestShootingPos()
    {
        if (spawners.Length > 0)
        {
            float minDis = Mathf.Infinity;
            Vector3 currentPos = transform.position;
            foreach (GameObject t in spawners)
            {
                float dis = Vector3.Distance(t.transform.position, currentPos);

                if (dis < minDis)
                {
                    minDis = dis;
                    currentDestination = t.transform;

                }
            }
        }
    }


    public void Shoot()
    {

        GameObject golla = Instantiate(EnemyGolla, shooterGun.position, shooterGun.rotation);



    }


    void InititializePlayer()
    {

        player = Object.FindObjectOfType<Player_Health>().gameObject;

    }


}
