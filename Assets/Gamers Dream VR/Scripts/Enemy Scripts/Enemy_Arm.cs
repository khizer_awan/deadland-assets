﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Collider))]
public class Enemy_Arm : MonoBehaviour {

	[Range(1,100)]
	public int impactDamage;

	 


	// Use this for initialization
	void Start () {
		GetComponent<Collider>().isTrigger = true;	
	}
	
	// Update is called once per frame
	void Update () {
		
	}



	public void OnTriggerEnter(Collider other){

		 Player_Health playerHealth = other.GetComponent<Player_Health> ();
 
		if (playerHealth) { 
			playerHealth.ApplyDamage (impactDamage);
		
		}


	}




}
