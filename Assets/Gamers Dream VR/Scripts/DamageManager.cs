using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Runtime.Remoting.Messaging;

public class DamageManager : MonoBehaviour
{

	public GameObject deadbody;

	public AudioClip[] hitsound;

	[Header("General Variables")]
	public float Health = 100;
	public float deathforce = 200f;
	public int Score = 10;

	public GameObject healthBar;

 
	void Awake(){

		healthBar = GetComponentInChildren<Canvas> ().GetComponentInChildren<Slider>().gameObject;

	}

	
	void Start(){
		if (healthBar) {
			healthBar.SetActive (false);
		} else {
			Debug.LogWarning ("there is no healthbar canvas attached to this object");
		}
	}
	
	void Update(){
 

		if (Health <= 0) {
			
		}
	}
	
	public void ApplyDamage (int damage, Vector3 velosity, float distance)
	{
		if (Health <= 0) {
			return;
		} 
		Health -= damage;
	}
	
	public void ApplyDamage (int damage, Transform gun)
	{
		if (Health <= 0) {
			return;
		}

		Health -= damage;
		StartCoroutine (showHealthBar(1f));
		if (Health <= 0) {
			Dead (gun);
		}




		
	}
	
	public void AfterDead ()
	{
		int scoreplus = Score;
		 
		GameManager manager = (GameManager)GameObject.FindObjectOfType (typeof(GameManager));	
		if(manager){
			manager.AddScore (scoreplus);
		}
	}
	
	
	public void Dead (Transform gun)
	{

		if (deadbody != null) {
			// this Object has removed by Dead and replaced with Ragdoll. the ObjectLookAt will null and ActionCamera will stop following and looking.
			// so we have to update ObjectLookAt to this Ragdoll replacement. then ActionCamera to continue fucusing on it.
			GameObject deadReplace = (GameObject)Instantiate (deadbody, this.transform.position, this.transform.rotation);
			// copy all of transforms to dead object replaced
			CopyTransformsRecurse (this.transform, deadReplace);
			Rigidbody[] rgd = deadReplace.GetComponentsInChildren<Rigidbody> ();

			foreach(Rigidbody bodypart in rgd){
				bodypart.AddForce (gun.forward*Random.Range(deathforce-50,deathforce+50));

			}
			// destroy dead object replaced after 5 sec
			Destroy (deadReplace, 5);
			// destry this game object.
			Destroy (this.gameObject,1);
			this.gameObject.SetActive(false);
		
		}
		AfterDead ();
	}
	
	// Copy all transforms to Ragdoll object
	public void CopyTransformsRecurse (Transform src, GameObject dead)
	{
		
		dead.transform.position = src.position;
		dead.transform.rotation = src.rotation;


		foreach (Transform child in dead.transform) {
			var curSrc = src.Find (child.name);

			if (curSrc) { 		
					
				CopyTransformsRecurse (curSrc, child.gameObject);
			}
		}
	}


	IEnumerator showHealthBar(float sec){

		healthBar.SetActive(true);
		healthBar.GetComponent<Slider>().value = Health;

		yield return new WaitForSeconds (sec);

		healthBar.SetActive(false);

	}
}
