﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GunPointer : MonoBehaviour {

    public Image itemImage;
    public float margin= 0.5f;
	public bool isGunPicked;


    private Transform ItemToPoint;

    Vector3 pos;
    Quaternion rot;

    Canvas pointer;

	// Use this for initialization
	void Start () {
        ItemToPoint = transform.parent;
        rot = ItemToPoint.localRotation;
	}
	
	// Update is called once per frame
	void Update () {



        if(!isGunPicked){
            pos = ItemToPoint.localPosition;
            pos.y += margin;
        transform.rotation = rot;
        transform.position = pos;
        }

      //  transform.LookAt(Camera.main.transform);
	}
}
