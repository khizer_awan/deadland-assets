﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsToPool : MonoBehaviour {


	public GameObject BulletHitStatic;
	public GameObject BulletHitBody;
	public GameObject BloodSplashEffect;

	PoolManager pool;
	// Use this for initialization
	void Start () {
		pool = GetComponent<PoolManager> ();
		pool.CreatePool (BulletHitStatic,50);
		pool.CreatePool (BulletHitBody,50);
		pool.CreatePool (BloodSplashEffect,20);

	}

	// Update is called once per frame
	void Update () {

	}
}
