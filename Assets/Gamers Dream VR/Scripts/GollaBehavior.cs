﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GollaBehavior : MonoBehaviour {


	public int GollaSpeed=10;
	public int GollaDamage=5;
	public float RayHitDistance =0.2f;
	public GameObject GollaHitEffect;
	public GameObject GollaIParticlesEffects;



	private Transform player;
	private Rigidbody rigidBody;
	private bool alreadyExploded = false;
	void Awake(){
 
		GollaHitEffect.SetActive (false);
		player = FindObjectOfType<Player_Health> ().transform;
	}

	// Use this for initialization
	void Start () {
		
		Destroy (gameObject, 10f);

		transform.LookAt (player.position);
		rigidBody = GetComponent<Rigidbody> ();
		rigidBody.AddForce (transform.forward*GollaSpeed);

		transform.LookAt (player.position);
 

	}


//	public void OnCollisionEnter(Collision other){
//	
//		hitEffect (other.collider);
//
//	
//	}


//	public void OnTriggerEnter(Collider other){
//		hitEffect (other);
//	} 


	void hitEffect(Collider other){

		rigidBody.isKinematic = true;

		GollaHitEffect.SetActive(true);

		Player_Health player_health = other.transform.GetComponent<Player_Health> ();
		if (player_health) {

			player_health.ApplyDamage (GollaDamage);

		}

		GollaIParticlesEffects.SetActive (false);
		Destroy (gameObject,3f);
	}

	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate(){
		
		if(!alreadyExploded){ 
 
		RaycastHit hit; 

		if (Physics.Raycast(transform.position, transform.forward, out hit, RayHitDistance)) {
				//print ("we have a hit "+hit.collider.name);
			hitEffect (hit.collider);
				alreadyExploded = true;
			
		} 

		}
	}

 

}
