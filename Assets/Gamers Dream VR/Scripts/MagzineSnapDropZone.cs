﻿namespace VRTK.Examples{
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagzineSnapDropZone : VRTK_SnapDropZone {


      //  public GameObject oldMagazine;
        public AssaultRifle rifle;
        public AudioClip magAttachSound;




		protected override void Awake()
		{
            base.Awake();
            
             if(rifle == null){
                print("rifle is null");
                rifle = transform.parent.GetComponent<AssaultRifle>();
            }
            
		}

	

		public override void OnObjectSnappedToDropZone(SnapDropZoneEventArgs e)
		{
            base.OnObjectSnappedToDropZone(e);
            

            MagazineBehavior mag = e.snappedObject.GetComponent<MagazineBehavior>();
            mag.GetComponent<Collider>().isTrigger = true;
            rifle.Refill(mag.totalMagazines);

            
		}


		public override void OnObjectUnsnappedFromDropZone(SnapDropZoneEventArgs e)
		{
            base.OnObjectUnsnappedFromDropZone(e);
           

            MagazineBehavior mag = e.snappedObject.GetComponent<MagazineBehavior>();
            mag.GetComponent<Collider>().isTrigger = false;
            mag.totalMagazines = rifle.totalMagzines;
            rifle.Refill(0);


            //if mag is empty, just destroy it.
            if(mag.totalMagazines < 1){
                Destroy(mag.gameObject,10f);
            }
            
		}

	}
}