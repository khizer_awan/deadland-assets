﻿

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
namespace VRTK.Examples
{
	

public class SceneController : MonoBehaviour {


		public Custom_Headset_CollisionFade playercontroller;


	// Use this for initialization
		private void Start () {

			print("start");

		if (GetComponent<VRTK_ControllerEvents>() == null)
		{
				
				VRTK_Logger.Error(VRTK_Logger.GetCommonMessage(VRTK_Logger.CommonMessageKeys.REQUIRED_COMPONENT_MISSING_FROM_GAMEOBJECT, "SceneController", "VRTK_ControllerEvents", "the same"));

				return;
		}

		//Setup controller event listeners
			GetComponent<VRTK_ControllerEvents>().TriggerPressed += new ControllerInteractionEventHandler(RestartLevel);
			GetComponent<VRTK_ControllerEvents>().TouchpadPressed += new ControllerInteractionEventHandler(mainMenu); 
			print("inited ");
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}


		private void RestartLevel(object sender, ControllerInteractionEventArgs e) {
			if (!playercontroller.isAlive) {
				print("reset sdcenbe");

				SceneManager.LoadScene ("working");

				//Application.LoadLevel(Application.loadedLevel());
			}
	}


		private void mainMenu(object sender, ControllerInteractionEventArgs e){

			if (!playercontroller.isAlive) {
				SceneManager.LoadScene ("menu");
				print("main menu");
			}
		}


}
}