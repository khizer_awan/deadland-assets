﻿// Bullet marker. Using to adding into any objects that you want to have a Camera Action.
using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;


#if UNITY_EDITOR
using UnityEditor;
#endif

public class AS_BulletHiter : MonoBehaviour {


	public GameObject BloodEffect;
	public GameObject HeadShotEffect;
	public AudioClip[] Sounds;
	 



	protected DamageManager damageManager;



	void Awake(){ 
		damageManager = transform.root.GetComponent<DamageManager> ();

		//GetComponent<Collider>().isTrigger = true;
//		print ("name of collider is "+col.name);

	}
 

	public virtual void OnHit(RaycastHit hit,Transform bullet,int damage){

		if (BloodEffect) {
//			GameObject decay = (GameObject)GameObject.Instantiate(BloodEffect,hit.point,Quaternion.identity);
//			decay.transform.forward = bullet.forward - (Vector3.up*0.2f);

			PoolManager.instance.ReuseObject(BloodEffect,hit.point,Quaternion.identity);

 
		}
	}

//	public void AddAudio(Vector3 point){
//		GameObject sound = new GameObject("SoundHit");
//		sound.AddComponent<AS_SoundOnHit>();
//		GameObject soundObj = (GameObject)GameObject.Instantiate(sound,point,Quaternion.identity);
//		soundObj.GetComponent<AS_SoundOnHit>().Sounds = Sounds;
//		GameObject.Destroy(soundObj,3);
//	}

	void OnDrawGizmos ()
	{
		#if UNITY_EDITOR

		Matrix4x4 rotationMatrix = Matrix4x4.TRS(transform.position, transform.rotation, transform.lossyScale);
		Gizmos.matrix = rotationMatrix;
		Gizmos.color = Color.green;
		Gizmos.DrawSphere (Vector3.zero, 0.1f);
		Handles.Label(transform.position, this.name);
		#endif
	}
}
