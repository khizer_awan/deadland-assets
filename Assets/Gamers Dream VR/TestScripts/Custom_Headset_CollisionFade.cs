﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace VRTK.Examples{
	
public class Custom_Headset_CollisionFade :  VRTK_HeadsetCollisionFade {

		public float playerHealth=100f;
		public float getDamage=10f;
		public Color dieColor;

		public GameObject endingBox;

		Transform player;
		[HideInInspector]
		public bool isAlive=true;



		private GameManager manager;

		void Start(){
			StartCoroutine (initComponentsAfterDelay ());
			//manager = FindObjectOfType<GameManager> ();
		}


		IEnumerator initComponentsAfterDelay(){
			yield return new WaitForSeconds (1f);
			//player = GameObject.FindGameObjectWithTag (CommonStrings.PLAYER_TAG).GetComponent<Transform> ();

		}

		protected override void OnHeadsetCollisionDetect (object sender, HeadsetCollisionEventArgs e)
		{
			base.OnHeadsetCollisionDetect (sender, e);

            //print("collided  "+e.collider.name);

 
			if (isAlive) {
				if (e.collider.tag.Equals (CommonStrings.ZOMBIE_HAND_TAG)) {
					if (playerHealth < 5) {
   
						isAlive = false; 
					//	Instantiate (endingBox,player);
//						Instantiate (endingBox, Vector3.zero, Quaternion.Euler(Vector3.zero), player);
						manager.playerDied ();
						StartCoroutine(gotoMenu ());
 
					} 
				}
			}
		}

		protected override void StartFade ()
		{
			base.StartFade ();
			if (playerHealth > 3) {
				playerHealth -= getDamage;


			}



		}

		IEnumerator gotoMenu(){
			yield return new WaitForSeconds (3f);
			SceneManager.LoadScene ("Menu");
		}


	 
	}
}
