﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieSpawner : MonoBehaviour {


	public int totalConcurrentZombies=5;
	public int SpawnPause;
	private float counter=0;

	public GameObject[] zombies;

	public GameObject[] spawnPoints;

	int totalSpawnPoints;
	int typesOfZombie;

	void Awake(){
		
	}


	// Use this for initialization
	void Start () {
		totalSpawnPoints = spawnPoints.Length;
		typesOfZombie = zombies.Length ;

		for(int i = totalConcurrentZombies; i > 0 ; i--){
		spawnZombie ();
		}
	}
	
	// Update is called once per frame
	void Update () {
		counter -= Time.deltaTime;

		if (counter < 0.1) {
			counter = SpawnPause;
			spawnZombie ();
		}


	}
 
	public void spawnZombie(){

		int zombIndex = Random.Range (0,typesOfZombie);
		int positionIndex = Random.Range (0,totalSpawnPoints);
		//print ("zombiew spawn  "+zombIndex+"   "+positionIndex+"    "+typesOfZombie+"   "+totalSpawnPoints);

//		print ("zombi type  " + zombies[zombIndex] + "      point  " + totalSpawnPoints);

		Instantiate (zombies[zombIndex],spawnPoints[positionIndex].transform.position,spawnPoints[positionIndex].transform.rotation);

	}




}
