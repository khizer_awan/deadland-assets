﻿

namespace VRTK.Examples
{ 
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
    using UnityEngine.UI;
    using TMPro;

	public class AssaultRifle : VRTK_InteractableObject {


	bool reloading = false;
	float time = 0;


//	TurretController controller;  

	[Header("Points")]
	[Tooltip("Point from which raycast should start")]
	public Transform aim;

	[Header("Ammo")]
	[Tooltip("Current available ammo of the gun")]
	public int ammo;
	[Tooltip("Magzine size of the Gun")]
	public int magzineSize = 50;
    [Tooltip("Total Magzines")]
    public int totalMagzines;
	//[Tooltip("Totale Ammo available for the GUN")]
	//public int totalAmmo = 500;
	[Tooltip("Time taken to reload the Gun")]
	public float reloadTime = 2f;
	[Tooltip("Damage done by the bullet")]
	public int ammoDamage = 10;

	[Header("Effects")]
	[Tooltip("Bullet Fire effect")]
	public ParticleSystem fireMuzzle;
	[Tooltip("Effect initialized at point where bullet hits")]
	public GameObject bulletHitStatic,bulletHitBody;

	[Header("Attack")]
	[Tooltip("Time after which next shot will be fired")]
	public float fireDelay = 0.1f;
	[Tooltip("Range of the Turret")]
	public float range = 20;


    [Header("Ammo UI")]
        public TextMeshProUGUI AmmoUI;
        public TextMeshProUGUI MagzUI;
        [Header("Pointer Canvas")]
        public GameObject pointerCanvas; // an image pointing towards this object when this game is somwhere on ground


        public bool isTesting =false;

	[Tooltip("All the Scripts reference on this object")]
	[Header("Components")]
	private AudioHandler _Audio;
		private bool firing=false;
        //private LineRenderer bulletTrail;
        //private bool showTrail;
		private VRTK_ControllerReference controllerReference;


	//Get the component
	void Start(){
            print("firing");
		//getting all the components on the object. Setting them before RUN state will be a better approach
		_Audio = this.GetComponent<AudioHandler> ();
            //bulletTrail = GetComponent<LineRenderer>();
			//totalMagzines = Mathf.FloorToInt(totalAmmo / magzineSize);
            AmmoUI.text = "" + ammo;
            MagzUI.text = "" + totalMagzines;
            //bulletTrail.SetPosition(0, aim.localPosition);
	}

		protected override void Update()
		{
			base.Update();
            print("firing Update");
		//check FireDelay after fire
		if(time <= fireDelay)
			time += Time.deltaTime;
	}

		public override void Grabbed(VRTK_InteractGrab grabbingObject)
		{
			base.Grabbed(grabbingObject);
			controllerReference = VRTK_ControllerReference.GetControllerReference(grabbingObject.controllerEvents.gameObject);
            pointerCanvas.SetActive(false);

		}

		public override void Ungrabbed(VRTK_InteractGrab previousGrabbingObject)
		{
			base.Ungrabbed(previousGrabbingObject);
			controllerReference = null;
            pointerCanvas.SetActive(true);
		}

		protected override void OnEnable()
		{
			base.OnEnable();
			controllerReference = null; 
		}



		public override void StartUsing (VRTK_InteractUse currentUsingObject)
		{
			base.StartUsing (currentUsingObject);
			firing=true;

		}

		public override void StopUsing (VRTK_InteractUse previousUsingObject)
		{
			base.StopUsing (previousUsingObject);
			firing = false;



		}


		protected override void FixedUpdate()
		{
			base.FixedUpdate();
            print("firing Fixed Update");
 
            if(isTesting)
			firing = Input.GetKey (KeyCode.Space);
		//		if (startPoint && lineRenderer && !controller._Health.isDestroyed) {
		if(firing){
                //			RaycastHit hit;

                //			float range = range; //get range from the ShootingSystem script

                //			if (Physics.Raycast (startPoint.position, startPoint.forward, out hit, range)) {
               // print("firing");
			Fire (aim);//if hit some point then shoot through shootingSystem Fire Function

			//			}
            }else{
                //bulletTrail.SetPosition(1, aim.position);
            }
	}


	public void Fire(Transform startPoint){ //with hit effect
			
		if (reloading)
			return;

		if (ammo > 0) {
				
			if (time > fireDelay) {

					ammo--;
                    AmmoUI.text = ""+ammo;
					//print ();

					if (VRTK_ControllerReference.IsValid(controllerReference) && IsGrabbed())
					{ 
						VRTK_ControllerHaptics.TriggerHapticPulse(controllerReference, 3f, 0.1f, 0.01f);
					} 
				fireMuzzle.Stop ();		
				fireMuzzle.Play ();

                    //Vector3 bulletHitPostion;

				RaycastHit hit;
				if (Physics.Raycast (startPoint.position, startPoint.forward, out hit, range)) {
					GameObject hitObject = hit.collider.gameObject;
						//GameObject hitObjectParent = hitObject.transform.root.gameObject;
					//	hitObjectParent.SendMessage("ApplyDamage", ammoDamage, SendMessageOptions.RequireReceiver);

						Hit_Body bodyhit =  hitObject.GetComponent<Hit_Body> ();
//						print ("hit object name "+hitObject);
						if (bodyhit != null) {
							bodyhit.OnHit (hit,transform,ammoDamage);
						}

//                      print ("name "+hitObject.name);
                    Quaternion rotation = Quaternion.FromToRotation(Vector3.up, hit.normal);
                   // GameObject clone;



                        if (hitObject.GetComponent<DamageManager>()) {
                   // clone = Instantiate (bulletHitBody, hit.point, rotation,hitObject.transform) as GameObject;
                            if (PoolManager.instance != null) {
                                PoolManager.instance.ReuseObject (bulletHitBody, hit.point, rotation);
                            }
                    } else {
                       // clone = Instantiate (bulletHitStatic, hit.point, rotation,hitObject.transform)  as GameObject;
                            if (PoolManager.instance != null) {
                                print("bullet");
                                PoolManager.instance.ReuseObject (bulletHitStatic, hit.point, rotation);
                            }
                    }

                        //bulletHitPostion = hit.point;
                    }//else{
                        //bulletHitPostion = Vector3.one;
                        //bulletHitPostion.z *= 100;
                    //}
					

                        //bulletTrail.SetPosition(1, bulletHitPostion);   
                    
                        

					

				time = 0;
				_Audio.Play_Fire ();
                }else{
                    
                    //bulletTrail.SetPosition(1, aim.position);
                }

		} else {
			_Audio.Play_OutOfAmmo ();


                if (totalMagzines > 0 )
                {
                    Reload();
                }
                else {
                    UpdateGunUI();
                }
		}
	}

 public void Refill( int magz){
            
            totalMagzines = magz;
            //print("on refill total magz  "+totalMagzines);
           // totalAmmo = totalMagzines * magzineSize;
            if(ammo < 1 && totalMagzines > 1){
            Reload();
            }else{
               UpdateGunUI();
            }

        }

 //

	//Reload Turret 
	public void Reload(){

		reloading = true;
		_Audio.Play_Reload ();
		StartCoroutine (ReloadAfterDelay ());

	}
	//Reload Turret after the delay
	IEnumerator ReloadAfterDelay (){

		yield return new WaitForSeconds (reloadTime);

		if (magzineSize > 0) {

			//totalAmmo -= magzineSize;
			ammo = magzineSize;
                totalMagzines--;

		} else {		
			//ammo = totalAmmo;
			//totalAmmo = 0;
            }

		reloading = false;
            UpdateGunUI();
        }

        void UpdateGunUI(){
            MagzUI.text = "" + totalMagzines;
            AmmoUI.text = "" + ammo;


        }

}
}

// public void Refill(int magz){

// totalMagzines = magz;
// totalAmmo = magzineSize*totalMagzines;
// //if(ammo < 2)
// Reload();


// }


// 	//Reload Turret 
// 	public void Reload(){

// 		reloading = true;
// 		_Audio.Play_Reload ();
// 		StartCoroutine (ReloadAfterDelay ());

// 	}
// 	//Reload Turret after the delay
// 	IEnumerator ReloadAfterDelay (){

// 		yield return new WaitForSeconds (reloadTime);

// 		if (totalAmmo - magzineSize > 0) {

// 			totalAmmo -= magzineSize;
// 			ammo = magzineSize;
//                 totalMagzines--;
//                 MagzUI.text = ""+totalMagzines;
//                 AmmoUI.text = "" + ammo;

// 		} else {		
// 			ammo = totalAmmo;
// 			totalAmmo = 0;
// 		}

// 		reloading = false;
// 	}
// }
// }