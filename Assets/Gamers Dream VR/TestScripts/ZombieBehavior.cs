﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SocialPlatforms;
using System;
using Valve.VR.InteractionSystem;

public class ZombieBehavior : MonoBehaviour {


	NavMeshAgent agent;
	Transform player;
	GameManager manager;


	public Animator anim;
	public float senseAreaRadius = 50f;
	public float stoppingDistance=2f;

	[HideInInspector]
	public bool isAlive;
	public bool isAgressive;


	private GameObject[] points;
	private int totalPoints;
	private GameObject currentDestin;
	private  float distance;

	private int walkIndex;

	private AudioSource speaker;


	void Awake(){
		agent = GetComponent<NavMeshAgent> ();
		manager = FindObjectOfType<GameManager> ();
		points = GameObject.FindGameObjectsWithTag (CommonStrings.SPAWN_POINT_TAG);
		speaker = GetComponent<AudioSource> ();
		StartCoroutine (playSound());
		//player = GameObject.FindGameObjectWithTag ("player").GetComponent<Transform> ();
	}

	// Use this for initialization
	void Start () {
		isAlive = true;
		StartCoroutine (initComponentsAfterDelay ());
		walkIndex = UnityEngine.Random.Range (1, 3);
		anim.SetInteger ("walk",walkIndex);
		totalPoints = points.Length;
		currentDestin = points [UnityEngine.Random.Range (0, totalPoints)];
		agent.SetDestination (currentDestin.transform.position);

	}

	IEnumerator initComponentsAfterDelay(){
		yield return new WaitForSeconds (1f);
		player = GameObject.FindGameObjectWithTag (CommonStrings.PLAYER_TAG).GetComponent<Transform> ();
}


	IEnumerator playSound(){
	

		yield return new WaitForSeconds(UnityEngine.Random.Range(1,7));

		speaker.Play();

	}

	// Update is called once per frame
	void Update () {
 

//		RaycastHit hit;
//		Ray ray = new Ray();
//		ray.origin = transform.position;
//

//		distance = Vector3.Distance (transform.position, player.position);

//		if (Physics.SphereCast (ray, senseAreaRadius, out hit)) {
//		if (Vector3.Distance (transform.position, player.position) < senseAreaRadius) {
//		
//			isAgressive = true;
//			currentDestin = player.gameObject;
//
//
//		
//		}

//		Ray ray = new Ray ();
//		ray.origin = transform.position;
//		RaycastHit hit;
//
//		if (Physics.SphereCast (transform.position, senseAreaRadius, Vector3.zero, out hit)) {
//			print ("raycast hit");
//			if (hit.collider.tag.Equals (CommonStrings.CAMERA_TAG)) {
//
//
//				isAgressive = true;
//				currentDestin.transform.position = player.position;
//				agent.SetDestination (player.position);
//			}
//		}
			
		//player dsetanion and distance calculation
		if (isAgressive) {
			if (player != null && isAlive) {
				if (Vector3.Distance (agent.gameObject.transform.position, player.position) < stoppingDistance) {
					//			if (agent.isStopped) {
					agent.destination = transform.position;
					anim.SetInteger ("walk",0);
					anim.SetBool ("attack", true);
				} else {
					agent.destination = player.position;
					anim.SetInteger ("walk",walkIndex);
					anim.SetBool ("attack", false);
				}
			}
			
		}

		//random point distance and destination calculation
		if (!isAgressive && currentDestin!=null) {
			if (Vector3.Distance (agent.gameObject.transform.position, currentDestin.transform.position) < 20) {
				currentDestin = points [UnityEngine.Random.Range (0, totalPoints)];
				agent.SetDestination (currentDestin.transform.position);
 
			}
 

		}


	}
		
	public void Died(){

		anim.SetBool ("die", true);
		agent.SetDestination( transform.position);
		isAlive = false;

		BoxCollider[] collider = GetComponentsInChildren<BoxCollider> ();

		foreach(BoxCollider col in collider){
			col.enabled = false;
		}
		manager.zombieDied ();

	}


	public void gotHit(){
		anim.SetTrigger ("hit");


	}

//
//	public void OnDrawGizmos() {
//		Gizmos.color = Color.yellow;
//		Gizmos.DrawSphere (transform.position,5f);
//	}
//

}
