﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR.InteractionSystem;
using System.Runtime.Remoting.Messaging;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {


	[Header("Zombie Prefabs")]
	public GameObject[] LandZombies;


	[Header("UI Elements")]
	public Text score;

	[Header("Spawn/Waypoints")]
	public Transform[] LandPoints;

	[Header("Variables")]
	public int maxZombies=30;



	//================Zombies Variables================//
	private int typesOfZombie;
	private int currentLandZombies;


	//================Points Variables================//
	private int totalPoints;
	private int currentPoints;


	//================Random Variables================//
	private int currentScore=0;
	private enum GameState{
		Playing,
		GameOver
	}
	private GameState gameState;


	// Use this for initialization
	void Start () {
		gameState = GameState.Playing;
		totalPoints = LandPoints.Length;
		typesOfZombie = LandZombies.Length ;


		StartCoroutine (InitZombies());

		//temporary for videography purposes
		Cursor.visible = false;
	}
	 


	public void zombieDied(){

		if (gameState == GameState.Playing) {

			SpawnZombie ();

			currentScore++;
			score.text = "Zombies Killed "+currentScore;
 
//			if(PlayerPrefs.GetInt (CommonStaticStrings.SCORE_TAG+SceneManager.GetActiveScene().name,0) < currentScore){
//				PlayerPrefs.SetInt (CommonStaticStrings.SCORE_TAG,currentScore);	
//			}
				
		}

	}


	public void SpawnZombie(){

		int zombIndex = Random.Range (0,typesOfZombie);
		int positionIndex = Random.Range (0,totalPoints);
//		Instantiate (zombies[zombIndex],spawnPoints[positionIndex].transform.position,spawnPoints[positionIndex].transform.rotation);

	} 


	public void playerDied(){
		gameState = GameState.GameOver;
	}



	IEnumerator InitZombies(){

		SpawnZombie ();

		yield return new WaitForSeconds (0.5f);


		if (currentLandZombies < maxZombies) {

			StartCoroutine (InitZombies());
		}

		currentLandZombies++;



	}



	public void AddScore (int score){

		currentScore += score;

		//ADD score to the databse or make some temp variables to store the score for showing on the leaderboard

	}


}
