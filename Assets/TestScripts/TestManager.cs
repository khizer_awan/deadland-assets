﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestManager : MonoBehaviour {

	public GameObject BulletHitStatic;
	public GameObject BulletHitBody;

	PoolManager pool;
	// Use this for initialization
	void Start () {
		pool =  FindObjectOfType<PoolManager> ();
		pool.CreatePool (BulletHitStatic,50);
		pool.CreatePool (BulletHitBody,50);


	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
