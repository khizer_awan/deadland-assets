﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestLookAtEnemy : MonoBehaviour {

	public Transform target;

	// Use this for initialization
	void Start () {
		transform.LookAt (transform.position);
	}
	
	// Update is called once per frame
	void Update () {

//		print ("test");
		transform.LookAt(target);
	}
}
